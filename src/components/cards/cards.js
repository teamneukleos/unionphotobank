import React from "react";
import "./card.css";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTrashAlt,
  faEdit,
  faArrowCircleDown,
} from "@fortawesome/free-solid-svg-icons";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { BrowserRouter as Router } from "react-router-dom";

import EditForm from "../upload/edit";
import { host } from "../../api/image";

class card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modaledit: false,
      modaldel: false,
      modaldownload: false,
      name: this.props.name,
      tags: this.props.tags,
      copyright: this.props.copyright,
      image: "...",
    };
  }

  componentDidMount() {
    // console.log(this.props)
  }

  downloadDB = () => {
    const config = {
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        responseType: "blob",
      },
    };
    axios
      .get(`${host}/image/download/${this.props.id}`, config) //arraybuffer
      .then((res) => {
        let blob = new Blob([res.data], { type: "application/octet-stream" }); //'application/pdf'
        let link = document.createElement("a");
        link.href = window.URL.createObjectURL(blob);
        link.download = `000${this.props.id}UB_RM.jpg`;
        link.click();

        // const url = window.URL.createObjectURL(new Blob([res.data]));
        // const link = document.createElement('a');
        // link.href = url;
        // link.setAttribute('download', 'picture.png'); //or any other extension
        // document.body.appendChild(link);
        // link.click();
      })
      .catch((err) => console.log(err));
  };

  deleteFromDB = () => {
    const config = {
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    };
    axios
      .delete(`${host}/image/${this.props.id}`, config)
      .then((res) => {
        console.log(res);
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  editImage = (e) => {
    e.preventDefault();
    // console.log(this.state);

    const data = this.getFormData(this.state);
    const config = {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    };
    axios
      .post(`${host}/image/${this.props.id}`, data, config)
      .then((res) => {
        window.location.reload();
      })
      .catch((error) => {});
  };

  getFormData = (object) => {
    const formData = new FormData();
    Object.keys(object).forEach((key) => formData.append(key, object[key]));
    return formData;
  };

  onFileChange = (e) => {
    let files = e.target.files || e.dataTransfer.files;
    if (!files.length) return;
    this.createFile(files[0]);
  };

  onInputChange = (ev) => {
    this.setState({ [ev.target.name]: ev.target.value });
  };

  createFile = (file) => {
    this.setState({ image: file });
    let reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        selectedimg: e.target.result,
      });
    };
    reader.readAsDataURL(file);
  };

  toggleModaledit = () => {
    this.setState((prevState) => ({
      modaledit: !prevState.modaledit,
    }));
  };
  toggleModaldel = () => {
    this.setState((prevState) => ({
      modaldel: !prevState.modaldel,
    }));
  };
  toggleModaldownload = () => {
    this.setState((prevState) => ({
      modaldownload: !prevState.modaldownload,
    }));
  };

  CloseModaledit = () => {
    this.setState((prevState) => ({
      modaledit: !prevState.modaledit,
    }));
  };
  CloseModaldel = () => {
    this.setState((prevState) => ({
      modaldel: !prevState.modaldel,
    }));
  };
  CloseModaldownload = () => {
    this.setState((prevState) => ({
      modaldownload: !prevState.modaldownload,
    }));
  };

  cvvv = (urll) => {
    var link = document.createElement("a");
    link.href = urll;
    link.download = "Download.jpg";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  checkRole = (x) => {
    if (x !== null) {
      let role = x;
      return role.split(",")[1];
    }
  };

  render() {
    return (
      <Router>
        <div className="col-6 col-md-3 card-wrap">
          {localStorage.getItem("role") === "user" ? (
            <div className="image" onClick={this.toggleModaldownload}>
              <div
                className="card-img"
                style={{
                  backgroundImage: `url(${host}${this.props.thumblink})`,
                }}
              ></div>
            </div>
          ) : (
            <div className="image">
              <div
                className="card-img"
                style={{
                  backgroundImage: `url(${host}${this.props.thumblink})`,
                }}
              ></div>
            </div>
          )}

          <div>
            <div className="left">
              <p className="subtitle">00{this.props.id}</p>
              <p className="item">{this.props.copyright}</p>
            </div>

            {localStorage.getItem("token") &&
            localStorage.getItem("role") === "admin" ? (
              <React.Fragment>
                <div className="right">
                  <span className="">
                    <Button
                      color="white"
                      className="caretButtonRed"
                      onClick={this.toggleModaldel}
                    >
                      <FontAwesomeIcon icon={faTrashAlt} />
                    </Button>
                  </span>
                  <span className="">
                    <Button
                      color="white"
                      className="mr-2 caretButton"
                      onClick={this.toggleModaledit}
                    >
                      <FontAwesomeIcon icon={faEdit} />
                    </Button>
                  </span>
                </div>
              </React.Fragment>
            ) : null}

            {localStorage.getItem("token") &&
            localStorage.getItem("role") === "user" ? (
              <React.Fragment>
                <div className="right">
                  {/* <a href={`${host}${this.props.image}`} download className=''><i className=''></i>
                  <FontAwesomeIcon icon={faArrowCircleDown} className="caretButtonRed"/>
                </a> */}
                  <Button
                    color="white"
                    className="caretButtonRed"
                    onClick={this.downloadDB}
                  >
                    <FontAwesomeIcon icon={faArrowCircleDown} />
                  </Button>
                </div>
              </React.Fragment>
            ) : null}
          </div>
        </div>

        {/* Edit Modal */}
        <Modal isOpen={this.state.modaledit} toggle={this.CloseModaledit}>
          <ModalHeader toggle={this.CloseModaledit}>
            <span className="formLabel bold">Edit Image</span>
          </ModalHeader>
          <form onSubmit={this.editImage}>
            <ModalBody>
              <EditForm
                editAction={this.editImage}
                image={this.props}
                fileChange={this.onFileChange}
                inputChange={this.onInputChange}
              />
            </ModalBody>
          </form>
        </Modal>

        {/* Delete Modal */}
        <Modal isOpen={this.state.modaldel} toggle={this.CloseModaldel}>
          <ModalBody style={{ fontSize: 16 }}>
            <span>Confirm you want to delete this image from the Library?</span>
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              className="btn btn-default"
              onClick={this.toggleModaldel}
            >
              Cancel
            </button>
            <button
              type="button"
              color="red"
              className="btn btn-danger"
              onClick={this.deleteFromDB}
            >
              Delete
            </button>
          </ModalFooter>
        </Modal>

        {/* Download Modal */}
        <Modal
          isOpen={this.state.modaldownload}
          toggle={this.CloseModaldownload}
        >
          <ModalBody style={{ fontSize: 16 }}>
            <div
              className="mb-1 modal-img"
              style={{ backgroundImage: `url(${host}${this.props.image})` }}
            ></div>
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              className="btn btn-default"
              onClick={this.toggleModaldownload}
            >
              Cancel
            </button>
            <Button
              type="button"
              color="red"
              className="btn btn-danger"
              onClick={this.downloadDB}
            >
              Download
            </Button>
          </ModalFooter>
        </Modal>
      </Router>
    );
  }
}

export default card;
