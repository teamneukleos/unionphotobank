import React, { Component } from "react";
import Card from "../cards/cards";
import { getAllImages } from "../../api/image";

import { Button, Modal, ModalHeader, ModalBody} from 'reactstrap';
import { BrowserRouter as Router } from "react-router-dom";
import UploadForm from '../upload/index';
import Header from "../header/header";

import Select from "react-dropdown-select";

import "./body.css";

import axios from 'axios';
import { host } from '../../api/image'


class Cards extends Component {

  constructor(props) {
    super(props);
    this.state = {
        showMenu: false,
        visibility: false,
        modalupload: false,
        modaldel: false,
        name: this.props.name,
        tags: this.props.tags,
        copyright: this.props.copyright,
        searchText: '',

        toggler: false,
        slide: 1,
        images: [],

        label: '',
        options: [
          { value: 'All', label: 'All' },
          { value: 'People', label: 'People' },
          { value: 'Culture', label: 'Culture' },
          { value: 'Environment', label: 'Environment' },
          { value: 'Business', label: 'Business' }
        ]
    };

    this.showMenu = this.showMenu.bind(this);
    this.closeMenu = this.closeMenu.bind(this);
    this.showform = this.showform.bind(this);
    this.hideform = this.hideform.bind(this);
  
  }

  showMenu(event) {
    event.preventDefault();
    this.setState({ showMenu: true }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }

  closeMenu(event) {
    if (!this.dropdownMenu.contains(event.target)) {
      this.setState({ showMenu: false }, () => {
        document.removeEventListener('click', this.closeMenu);
      });
    }
  }
 
  showform(event){
    event.preventDefault();
    this.setState({
       visibility: !this.state.visibility });
  }

  hideform(event) {
    if (!this.displayform.contains(event.target)) {
      this.setState({ visibility: false }, () => {
        document.removeEventListener('click', this.hideform);
      });
    }
  }

  toggleModalcreate = () => {
    this.setState(prevState => ({
      modalupload: !prevState.modalupload,
    }));
  }

  toggleModalupload = () => {
    this.setState(prevState => ({
      modalupload: !prevState.modalupload,
    }));
  }


  CloseModalupload = () => {
    this.setState(prevState => ({
      modalupload: !prevState.modalupload,
    }));
  }

  logout = () => {
    // localStorage.removeItem('token')
    localStorage.clear();
    window.location.reload()
  }



  componentDidMount = () => {
    getAllImages().then(res => {
        return this.setState({
          images: res.data}
        );
    });
  };


  openLightboxOnSlide = slideNumber => {
    this.setState({
      toggler: !this.state.toggler,
      slide: slideNumber
    });
  };


  //after first works/second may not because this.state.images will have been altered, except there's a stop gap container
  sortbytag = (text) => {
    var sorter = text[0].value
    var sorted = this.state.images.filter(res => res.tags === sorter)
    this.setState({
      label: sorter, 
      images: sorted
    });
  };


  searchImage = async(searchText) =>  {
    const config = {
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem("token")}`
      }
    };
    axios.post(`${host}/api/imageSearch/${searchText}`, config)
      .then(res => {
        return this.setState({
          images: res.data}
        );
      })
      .catch((error) => {
       this.setState({ images: [] })
      });
  }


  handleCategory = (text) => {
    var sorter = (text[0].value).toLowerCase()

    if (sorter !== 'all') {
      try {
        this.setState({label: sorter})
        this.searchImage(sorter)
      } catch (error) {
      }
    }
    else if (sorter === 'expired') {
    
    }

    getAllImages().then(res => {
        return this.setState({
          images: res.data}
        );
    });
  }

  // handleChange = (e) => {
  //   e.preventDefault()
  //   this.setState({label:e.target.value});
  // }

  
  checkRole =(x)=>{
    if (x !== null){
      let role = x
      return role.split(',')[1]
    }
  }

  handleSearch = e => {
    if (e.target.value !== '') {
      this.setState({ searchText: e.target.value }); // let x = this.refs.namedHere.value; //another way to get element values by setting refs
      this.searchImage(e.target.value)
    }

    getAllImages().then(res => {
        return this.setState({
          images: res.data}
        );
    });
  }
  

  render() {
    const { images } = this.state;
    return (
      <Router>
          <header>
              <Header value={this.state.searchText} searchAction={this.handleSearch} className="App-header" />
          </header>
      <div>
        <div className = "mt-4 toolHolder">
          
          <span className="actionButtons">
            {/* <button onClick={this.sortbytag('People')} className="btn btn-outline dropdown-toggle cvv" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {this.state.label}
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
              <button class="dropdown-item" type="button">People</button>
              <button class="dropdown-item" type="button">Women</button>
              <button class="dropdown-item" type="button">Buildings</button>
            </div>

            <select value={this.state.label} 
              onChange={this.handleChange}
            >
              <option value="Orange">Orange</option>
              <option value="Radish">Radish</option>
              <option value="Cherry">Cherry</option>
            </select> */}

            {/* {
              localStorage.getItem("token") && localStorage.getItem("role") === 'admin' ? 
              <React.Fragment>
                <Button outline color="white" className="mr-3 upload-btn" onClick={this.toggleModalcreate}>Upload</Button> 
                <a href="#" className="text-black"> <div className="mr-5" onClick ={this.logout}>Log Out</div></a> 
              </React.Fragment>
              :
              <Select
                className="cvv"
                options={this.state.options}
                // values={[]}
                placeholder={"Sort by Categories"}
                separator={true}
                // onChange={(values) => this.sortbytag(values)}
                onChange={(values) => this.handleCategory(values)}
              /> 
               // <Button outline color="white" className="mr-5 upload-btn"> <a href="/login">Login</a></Button>
            } */}

            <Select
              className="cvv"
              options={this.state.options}
              // values={[]}
              placeholder={"Sort by Categories"}
              separator={true}
              // onChange={(values) => this.sortbytag(values)}
              onChange={(values) => this.handleCategory(values)}
            />
          </span>

          {
            images.length>0 ? <span className="ml-4 imageLeft number">{images.length} Images</span>  : "" //An if/else statement to show image count WHEN MORE THAN 0
          }

            {/* Below commented in move of Login/Logout/Upload buttons to right */}
            {/* {
              localStorage.getItem("token") && localStorage.getItem("role") === 'admin' ? 
              <React.Fragment>
                <Button outline color="green" className="ml-3 mr-1 upload-btn" onClick={this.toggleModalcreate}>Upload</Button> 
                <a href="#" className="text-black"> <Button outline color="danger" className="ml-2 mr-3 upload-btn" onClick ={this.logout}>Logout</Button></a>  
              </React.Fragment>
              :
              null 
            } */}

            {/* {
              localStorage.getItem("token") && localStorage.getItem("role") === 'user' ? 
              <React.Fragment>
                // <a href="#" className="text-black"> <div className="ml-3 droptext" onClick ={this.logout}>Log Out</div></a> 
                <a href="#" className="text-black"> <Button outline color="danger" className="ml-3 mr-3 upload-btn" onClick ={this.logout}>Logout</Button></a>  
              </React.Fragment>
              : 
              null 
            } */}

            {/* {
              localStorage.getItem("token") === null ? 
              <React.Fragment>
                <a href="/login" className="text-black"> <Button outline color="white" className="ml-3 mr-3 upload-btn">Login</Button></a>  
              </React.Fragment>
              : 
              null 
            } */}


          <span className="rightJust">
            {/* {
              images.length>0 ? <div className="rightJust number">{images.length} Images</div>  : "" //An if/else statement to show image count WHEN MORE THAN 0
            } */}

            {
              localStorage.getItem("token") === null ? 
              <React.Fragment>
                <a href="/login" className="text-black"> <Button outline color="white" className="ml-3 upload-btn">Login</Button></a>  
              </React.Fragment>
              : 
              null 
            }

            {
              localStorage.getItem("token") && localStorage.getItem("role") === 'user' ? 
              <React.Fragment>
                <a href="/" className="text-black"> <Button outline color="danger" className="ml-3 upload-btn" onClick ={this.logout}>Logout</Button></a>  
              </React.Fragment>
              : 
              null 
            }

            {
              localStorage.getItem("token") && localStorage.getItem("role") === 'admin' ? 
              <React.Fragment>
                <Button outline color="green" className="ml-3 mr-1 upload-btn hideonmobile" onClick={this.toggleModalcreate}>Upload</Button> 
                <a href="/" className="text-black"> <Button outline color="danger" className="ml-2 upload-btn" onClick ={this.logout}>Logout</Button></a>  
              </React.Fragment>
              :
              null 
            }
          </span>

        </div>


        <div className="card-grid">
            {
            localStorage.getItem("token") && localStorage.getItem("role") === 'admin' ? 
              images.map(
                res => 
                  <Card key={res.id}
                    image={`${res.image_link}`}
                    thumblink={`${res.thumblink}`}
                    name={res.name}
                    tags={res.tags}
                    copyright={res.copyright}
                    id={res.id}

                    showLightbox={this.openLightboxOnSlide.bind(this, `${res.id}`)}
                  />
              )
              :
              ( images.filter(e => e.copyright > new Date().getFullYear()) ).map(
              // images.map(
                res => 
                  <Card key={res.id}
                    image={`${res.image_link}`}
                    thumblink={`${res.thumblink}`}
                    name={res.name}
                    tags={res.tags}
                    copyright={res.copyright}
                    id={res.id}

                    showLightbox={this.openLightboxOnSlide.bind(this, `${res.id}`)}
                  />
              )
            }
        </div>
        
      </div>

      {/* Upload Modal */}
      <Modal isOpen={this.state.modalupload} toggle={this.CloseModalupload}>
          <ModalHeader toggle={this.CloseModalupload}>
            <span className='formLabel bold'>Upload Image</span>
          </ModalHeader>
        <form onSubmit={this.uploadFile}>
          <ModalBody>   
            <UploadForm />
          </ModalBody>
        </form>
      </Modal>

      </Router>
    );
  }
}

export default Cards;
