import React, { Component } from "react";
import { FormGroup, Label} from 'reactstrap';
import axios from "axios";

// import { imageSave } from "../../api/auth";
import { host } from '../../api/image'

class UploadForm extends Component {

  constructor(){
    super()
    this.uploadFile = this.uploadFile.bind(this)
  }
  state = {
    // upload: "",
    tags: "",
    copyright: "",
    name: "",
    image: ""
  };

  async uploadFile(e) {
    e.preventDefault()

    const data = this.getFormData(this.state);

    const config = {
      headers: {
        Accept: "application/json",
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    };
    axios.post(`${host}/api/imageUpload`, data, config)
      .then(res => {
        window.location.reload();
      })
      .catch((error) => {
      });
  }


  onFileChange = (e) => {
    let files = e.target.files || e.dataTransfer.files;
    if (!files.length)
      return;
    this.createFile(files[0]);
  }
  onEditFileChange = (e) => {
    let files = e.target.files || e.dataTransfer.files;
    if (!files.length)
      return;
    this.createEditFile(files[0]);
  }
  createFile = (file) => {
    this.setState({ image: file })
    let reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        selectedimg: e.target.result
      })
    };
    reader.readAsDataURL(file);
  }

  getFormData = (object) => {
    const formData = new FormData()
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData
  }

  onInputChange = ev => {
    this.setState({ [ev.target.name]: ev.target.value });
  };

  render() {
    return (
      <form onSubmit={this.uploadFile} className="full">

        <input type="file" name="image" onChange={this.onFileChange} isRequired/>

        <FormGroup>
        <Label for="Name">Tags</Label>
        <input type="text" name="tags" className="form-control" placeholder="Enter upto 8 tags, each seperated by commas" onChange={this.onInputChange} isRequired/>
        </FormGroup>

        <FormGroup>
        <Label for="Name">Copyright</Label>
        <input type="text" name="copyright" className="form-control" placeholder="Copyright" onChange={this.onInputChange} isRequired/>
        </FormGroup>

        <button type="submit" class="btn btn-success">Upload File</button>
      </form>
    );
  }
}
export default UploadForm;
