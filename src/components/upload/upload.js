import React from 'react';
import axios from 'axios';
import { host } from '../../api/image'


class FileUploadForm extends React.Component {

    constructor(props) {
        super(props);
        this.state ={
          file:null
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
        this.uploadFile = this.uploadFile.bind(this)
    }

    async uploadFile(file){
      
        const data = this.getFormData(this.state)
        // console.log(data.getAll("image"))
        const config = {
          headers: {
            'Accept': 'application/json',
            //'Content-Type': 'application/json', 
            'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
        };

    axios.post(`${host}/api/imageUpload`, data, config).then(res => {
      if (this.state.title !== '') {
        if (res.data.message === "created") {
          this.setState({
            visible: true, hasError: false, notifyType: 'add',
            current_collections: [...this.state.current_collections, res.data.data],
          });
          setTimeout(() => { this.setState({ visible: false, hasError: false }) }, 3000);
        }
        else {
          this.setState({ visible: true, hasError: true, notifyType: 'add' });
        }
      }
      }).catch(() => {
        this.setState({ visible: true, hasError: true, notifyType: 'add' });
        setTimeout(() => { this.setState({ visible: false, hasError: false }) }, 3000);
      }
    )};
    onChange(e) {
        this.setState({file:e.target.files[0]})
    }


    onFileChange = (e) => {
        let files = e.target.files || e.dataTransfer.files;
        if (!files.length)
          return;
        this.createFile(files[0]);
      }
      onEditFileChange = (e) => {
        let files = e.target.files || e.dataTransfer.files;
        if (!files.length)
          return;
        this.createEditFile(files[0]);
      }
      createFile = (file) => {
        this.setState({ image: file })
        let reader = new FileReader();
        reader.onload = (e) => {
          this.setState({
            selectedDoc: e.target.result
          })
        };
        reader.readAsDataURL(file);
      }

    onFileChange = (ev) => {
    }
    getFormData = (object) => {
      const formData = new FormData()
      Object.keys(object).forEach(key => formData.append(key, object[key]));
      return formData
    }

    //   addDocument = () => {
    //     console.log(this.state)
      
    //     const data = this.getFormData(this.state)
    //     console.log(data.getAll("image"))
    //     const config = {
    //       headers: {
    //         'Accept': 'application/json',
    //         //'Content-Type': 'application/json',
    //         'Authorization': `Bearer ${localStorage.getItem('token')}`
    //       }
    //     }
      render() {
        return (
          <form onSubmit={this.uploadFile}>
        <h6> File Upload Sample</h6>
        <input type="file" name="file" onChange={this.onFileChange} />
        <input
          type="text"
          name="name"
          placeholder="name"
          onChange={this.onInputChange}
        />
        <input
          type="text"
          name="tags"
          placeholder="Tags"
          onChange={this.onInputChange}
        />
        <input
          type="text"
          name="copyright"
          placeholder="copyright"
          onChange={this.onInputChange}
        />
        <button type="submit">Upload File</button>
      </form>
       )
      }
        
}

export default FileUploadForm;