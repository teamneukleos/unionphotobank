import React from "react";
import { FormGroup, Label } from "reactstrap";

const EditForm = ({ editAction, fileChange, inputChange, image }) => {
  return (
    <form onSubmit={editAction} className="full">
      <input type="file" name="image" onChange={fileChange} />

      <FormGroup>
        <Label for="Name">Tags</Label>
        <input
          type="text"
          name="tags"
          defaultValue={image.tags}
          className="form-control"
          placeholder="Enter upto 8 tags, each seperated by commas"
          onChange={inputChange}
        />
      </FormGroup>

      <FormGroup>
        <Label for="Name">Copyright</Label>
        <input
          type="text"
          defaultValue={image.copyright}
          name="copyright"
          className="form-control"
          placeholder="Copyright"
          onChange={inputChange}
        />
      </FormGroup>

      <button type="submit" class="btn btn-success">
        Save
      </button>
    </form>
  );
};

export default EditForm;
