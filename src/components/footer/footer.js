import React from 'react';
// import { BrowserRouter as Router, Link } from "react-router-dom";
import './footer.css';
import TC from '../../assets/tandc.pdf'
import PP from '../../assets/privacypolicy.pdf'


import { checkRole } from '../../api/auth';



export default class Footer extends React.Component{  

  logout = () => {
    // localStorage.removeItem('token')
    localStorage.clear();
    window.location.reload()
  }

    render(){
      return (
        // <Router>
            // <div className="footer container text-center">
            <div className="footer">
              <span className="Arr">&copy; All Rights Reserved, {new Date().getFullYear()}</span>
              {/* <a href={'/login'} className="faq"> Login </a> */}
              <a href={TC} className="TC"> Terms and Conditions </a>
              <a href={PP} className="Policy"> Privacy Policy </a>
              {(localStorage.getItem('token') !== null && localStorage.getItem('role') === 'admin') ?
                <a href={'/forgot'}> Change Password </a> 
              : null}
            </div>
        // </Router>  
      ); 
    }
  }

