import React from 'react';
import './header.css';
import { BrowserRouter as Router, Link } from "react-router-dom";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars,faSearch } from '@fortawesome/free-solid-svg-icons';
import UploadForm from '../upload/index';

import { getAllImages } from "../../api/image";
import headerlogo from "../../assets/unionlogo.png"



export default class HeaderCopy extends React.Component{ 
 
  constructor(props) {
    super(props);
    this.state = {
      showMenu: false,
      visibility: false,
        modaledit: false,
        modaldel: false,
        name: this.props.name,
        tags: this.props.tags,
        copyright: this.props.copyright,
        image: '...',  
    };
    this.showMenu = this.showMenu.bind(this);
    this.closeMenu = this.closeMenu.bind(this);
    this.showform = this.showform.bind(this);
    this.hideform = this.hideform.bind(this);
  
  }
  showMenu(event) {
    event.preventDefault();
    this.setState({ showMenu: true }, () => {
      document.addEventListener('click', this.closeMenu);
    });
  }

  closeMenu(event) {
    if (!this.dropdownMenu.contains(event.target)) {
      this.setState({ showMenu: false }, () => {
        document.removeEventListener('click', this.closeMenu);
      });
    }
  }
 
  showform(event){
    event.preventDefault();
    this.setState({
       visibility: !this.state.visibility });
  }

  hideform(event) {
    if (!this.displayform.contains(event.target)) {
      this.setState({ visibility: false }, () => {
        document.removeEventListener('click', this.hideform);
      });
    }
  }

  toggleModalcreate = () => {
    this.setState(prevState => ({
      modaledit: !prevState.modaledit,
    }));
  }

  toggleModaledit = () => {
    this.setState(prevState => ({
      modaledit: !prevState.modaledit,
    }));
  }


  CloseModaledit = () => {
    this.setState(prevState => ({
      modaledit: !prevState.modaledit,
    }));
  }

  logout = () => {
    // localStorage.removeItem('token')
    localStorage.clear();
    window.location.reload()
  }


  componentDidMount = () => {
    // getAllImages().then(res => {
    //   console.log(res.data, "=====");
    //   return this.setState({
    //     images: res.data}
    //   );
    // });
  };
 

    render(){

      return ( 
        <Router>

          <div>
            <nav className="navbar navbar-light">

                <span className="col-3 menu-handles">
                   {/* <FontAwesomeIcon icon={faBars} className="hamburger" /> */}
                   {/* <FontAwesomeIcon icon={faSearch} className="search" onClick={this.showform}/> */}
                   <input ref={(element) => { this.displayform = element; }} id="searchInput" className="searchReady" type="search" placeholder="Search"/>
                </span>

                <span className="col-6 search-items">
                  {/* {
                    this.state.visibility
                    ?
                    (<input ref={(element) => { this.displayform = element; }} id="searchInput" className="searchReady" type="search" placeholder="SEARCH FOR IMAGE"/>
                    )
                    :
                    (<Link to={'/home'}><h2 className="union-title">Union<span className="union-faded">Pics</span></h2></Link>
                    )
                  } */}
                  <Link to={'/'}><h2 className="union-title">Studio<span className="union-faded">Photos</span></h2></Link>
                </span>
               
                <span className="col-3">
                  {/* {
                    localStorage.getItem("token") ? 
                    <React.Fragment>
                      <Button outline color="white" className="mr-3 upload-btn" onClick={this.toggleModalcreate}>Upload</Button> 
                      <a href="#" className="text-white"> <div className="mr-5"  onClick ={this.logout}>Log Out</div></a> 
                    </React.Fragment>
                    : null
                    // <Button outline color="white" className="mr-5 upload-btn"> <a href="/login">Login</a></Button>
                  } */}
                  <img src={headerlogo} className="union-logo" alt="" />
                </span>

            </nav>
          </div>

          {/* Upload Modal */}
          {/* <Modal isOpen={this.state.modaledit} toggle={this.CloseModaledit}>
              <ModalHeader toggle={this.CloseModaledit}><span className='formLabel bold'>Upload Image</span></ModalHeader>
              <form onSubmit={this.uploadFile}>
                <ModalBody>   
                  <UploadForm />
                </ModalBody>
                <ModalFooter>
                  <button type="button" className="btn btn-default" onClick={this.toggleModaledit}>Close</button>
                </ModalFooter>
              </form>
          </Modal> */}

        </Router> 
      ) 
    }
  }

