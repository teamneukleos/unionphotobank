import React from 'react';
import './header.css';
import { BrowserRouter as Router, Link } from "react-router-dom";

import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars,faSearch } from '@fortawesome/free-solid-svg-icons';
import UploadForm from '../upload/index';

import { getAllImages } from "../../api/image";
// import { Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faBars,faSearch } from '@fortawesome/free-solid-svg-icons';
// import UploadForm from '../upload/index';

// import { getAllImages } from "../../api/image";
import logo from '../../assets/unionlogo.png'



export default class Header extends React.Component{ 
 
  constructor(props) {
    super(props);
    this.state = {
      showMenu: false,
      visibility: false,
        modaledit: false,
        modaldel: false,
        name: this.props.name,
        tags: this.props.tags,
        copyright: this.props.copyright,
        image: '...',  
    };
    // this.showMenu = this.showMenu.bind(this);
    // this.closeMenu = this.closeMenu.bind(this);
    // this.showform = this.showform.bind(this);
    // this.hideform = this.hideform.bind(this);
  
  }
  // showMenu(event) {
  //   event.preventDefault();
  //   this.setState({ showMenu: true }, () => {
  //     document.addEventListener('click', this.closeMenu);
  //   });
  // }

  // closeMenu(event) {
  //   if (!this.dropdownMenu.contains(event.target)) {
  //     this.setState({ showMenu: false }, () => {
  //       document.removeEventListener('click', this.closeMenu);
  //     });
  //   }
  // }
 
  // showform(event){
  //   event.preventDefault();
  //   this.setState({
  //      visibility: !this.state.visibility });
  // }

  // hideform(event) {
  //   if (!this.displayform.contains(event.target)) {
  //     this.setState({ visibility: false }, () => {
  //       document.removeEventListener('click', this.hideform);
  //     });
  //   }
  // }

  // toggleModalcreate = () => {
  //   this.setState(prevState => ({
  //     modaledit: !prevState.modaledit,
  //   }));
  // }

  // toggleModaledit = () => {
  //   this.setState(prevState => ({
  //     modaledit: !prevState.modaledit,
  //   }));
  // }


  // CloseModaledit = () => {
  //   this.setState(prevState => ({
  //     modaledit: !prevState.modaledit,
  //   }));
  // }

  // logout = () => {
  //   localStorage.removeItem('token')
  //   window.location.reload()
  // }


  // componentDidMount = () => {
  //   // getAllImages().then(res => {
  //   //   console.log(res.data, "=====");
  //   //   return this.setState({
  //   //     images: res.data}
  //   //   );
  //   // });
  // };
 

    render(){

      return ( 
        <Router>

          <div>
            <nav className="navbar navbar-light">

                <span className="col-8 col-sm-8 col-md-4 col-lg-3 menu-handles">
                   <input onChange={this.props.searchAction} id="searchInput" /* className="searchReady" */ type="search" placeholder="Search"/>
                </span>

                <span className="col-1 col-sm-1 col-md-4 col-lg-6">
                  <Link to={'/'}><h2 className="union-title">Studio <span className="union-faded">Photos</span></h2></Link>
                </span>
               
                <span className="col-3 col-sm-3 col-md-4 col-lg-3 no-left-padding">
                  <img src={logo} className="union-logo" alt="" />
                </span>

            </nav>
          </div>

        </Router> 
      ) 
    }
  }

