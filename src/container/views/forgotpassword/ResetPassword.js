import React, { Component } from "react";
// import Header from "../../../components/header/header";
// import Footer from "../../../components/footer/footer";
import { PasswordResetToken, checkRole } from "../../../api/auth";
import "../login/login.css";
import { withRouter } from "react-router";

class ResetPassword extends Component {
  componentDidMount() {
    if (
      localStorage.getItem("token") !== null &&
      localStorage.getItem("role") === "admin"
    ) {
      return;
    } else {
      this.props.history.push("/");
    }
  }

  state = {
    visible: false,
    errorMessage: "",
  };
  onDismiss = () => {
    this.setState({ visible: false });
  };
  handleTextChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    PasswordResetToken(this.props.match.params.token, this.state).then((res) => {
      if (res.data === null) {
        this.setState({ visible: true, errorMessage: "Invalid credentials" });
        return;
      }
      if (res.data !== null) {
        // localStorage.setItem('token', res.data); Updated Passoword
        window.location.href = "/";
        return;
      } else {
        this.setState({
          visible: true,
          errorMessage: "Something went wrong, could not change password",
        });
        return;
      }
    });
  };

  render() {
    const { match, location, history } = this.props;
    return (
      <div>
        <div className="login">
          {/* <h2 className="login-header">Log in</h2> */}
          <form className="login-container" onSubmit={this.handleSubmit}>
            {/* <p>
            <input type="email" placeholder="Email"  name="email"  onChange={this.handleTextChange}  required/>
          </p> */}
            <p>
              <input
                type="password"
                placeholder="New Password"
                name="password"
                onChange={this.handleTextChange}
                required
              />
            </p>
            <p>
              <input
                type="password"
                placeholder="Confirm Password"
                name="password_confirmation"
                onChange={this.handleTextChange}
                required
              />
            </p>
            <p>
              <input type="submit" defaultValue="Change Password" />
              <a href={"/"}>
                <input type="button" defaultValue="BACK" />
              </a>
            </p>
          </form>
        </div>
      </div>
    );
  }
}
export default withRouter(ResetPassword);
