import React, { Component } from "react";
// import Header from "../../../components/header/header";
// import Footer from "../../../components/footer/footer";
import { login } from "../../../api/auth";
import "./login.css";

class Login extends Component {
  componentDidMount() {
    if (localStorage.getItem("token") !== null) {
      this.props.history.push("/");
    }
  }

  state = {
    visible: false,
    errorMessage: "",
  };
  onDismiss = () => {
    this.setState({ visible: false });
  };
  handleTextChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    login(this.state).then((res) => {
      if (res.data === null) {
        this.setState({ visible: true, errorMessage: "Invalid credentials" });
        return;
      }
      if (res.data !== null) {
        console.log(res.data);
        localStorage.setItem("token", res.data.token);
        localStorage.setItem("role", res.data.role);
        window.location.href = "/";
        return;
      } else {
        this.setState({
          visible: true,
          errorMessage: "Something went wrong, could not login",
        });
        return;
      }
    });
  };

  render() {
    return (
      <div>
        <div className="login">
          {/* <h2 className="login-header">Log in</h2> */}
          <form className="login-container" onSubmit={this.handleSubmit}>
            <p>
              <input
                type="email"
                placeholder="Email"
                name="email"
                onChange={this.handleTextChange}
                required
              />
            </p>
            <p>
              <input
                type="password"
                placeholder="Password"
                name="password"
                onChange={this.handleTextChange}
                required
              />
            </p>
            <p>
              <input type="submit" defaultValue="Log in" />
            </p>
          </form>
        </div>
      </div>
    );
  }
}
export default Login;
