import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Homepage from "../container/views/homepage/home";
import Login from "../container/views/login/login";
import ChangePassword from "../container/views/login/password";
import NotFound from "../container/404";
// import Header from "../components/header/header";
import Footer from "../components/footer/footer";
import ForgotPassword from "../container/views/forgotpassword/ForgotPassword";
import ResetPassword from "../container/views/forgotpassword/ResetPassword";
// wrappers

export default () => (
  <div className="home">
    <BrowserRouter>
      {/* <header>
                <Header className="App-header" />
            </header> */}
      <div>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Login} />
          <Route exact path="/password" component={ChangePassword} />
          <Route exact path="/reset/:token" component={ResetPassword} />
          <Route exact path="/forgot" component={ForgotPassword} />
          <Route exact path="/" component={Homepage} />
          <Route path="*" component={NotFound} />
        </Switch>
      </div>
      <Footer />
    </BrowserRouter>
  </div>
);
