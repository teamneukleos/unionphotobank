import { host } from "../api/image";

export const login = (data) =>
  fetch(`${host}/user/login`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  }).then((res) => {
    let data = res.json();
    if (res.status >= 200 && res.status < 300) {
      return data;
    } else {
      return data.then((err) => {
        throw err;
      });
    }
  });

export const user = (token) =>
  fetch(`${host}/user`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  }).then((res) => {
    let data = res.json();
    if (res.status >= 200 && res.status < 300) {
      return data;
    } else {
      return data.then((err) => {
        throw err;
      });
    }
  });

export const register = (credentials) =>
  fetch(`${host}/user/create`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((res) => {
    let data = res.json();
    if (res.status >= 200 && res.status < 300) {
      return data;
    } else {
      return data.then((err) => {
        throw err;
      });
    }
  });

export const resetPassword = (credentials) =>
  fetch(`${host}/user/password/update`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      // 'Authorization': `Bearer ${checkApi(localStorage.getItem("token"))}`
    },
    body: JSON.stringify(credentials),
  }).then((res) => {
    let data = res.json();
    if (res.status >= 200 && res.status < 300) {
      return data;
    } else {
      return data.then((err) => {
        throw err;
      });
    }
  });

// export const resetPassword = (credentials) =>
// fetch(`${host}/user/password/reset`, {
//     method: 'POST',
//     headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json'
//     },
//     body: JSON.stringify(credentials)
// }).then(res => {
//     let data = res.json();
//     if (res.status >= 200 && res.status < 300) {
//         return data;
//     } else {
//         return data.then(err => { throw err; });
//     }
// });

export const sendPasswordResetEmail = (data) =>
  fetch(`${host}/user/password/forgot`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  }).then((res) => {
    let data = res.json();
    if (res.status >= 200 && res.status < 300) {
      return data;
    } else {
      return data.then((err) => {
        throw err;
      });
    }
  });

export const PasswordResetToken = (token, data) =>
  fetch(`${host}/user/password/reset/${token}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  }).then((res) => {
    let data = res.json();
    if (res.status >= 200 && res.status < 300) {
      return data;
    } else {
      return data.then((err) => {
        throw err;
      });
    }
  });

export const imageSave = (data) =>
  fetch(`${host}/api/imageUpload`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      // 'Content-Type': 'application/json'
      // Authorization: `Bearer ${localStorage.getItem("token")}`
    },
    body: JSON.stringify(data),
  }).then((res) => {
    let data = res.json();
    if (res.status >= 200 && res.status < 300) {
      window.location.reload();
      return data;
    } else {
      return data.then((err) => {
        throw err;
      });
    }
  });

export const checkRole = (x) => {
  if (x !== null) {
    let role = x;
    return role.split(",")[1];
  }
};

const checkApi = (x) => {
  if (x !== null) {
    let role = x;
    return role.split(",")[0];
  }
};
