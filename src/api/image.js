// export const host = "http://localhost:5000/api";
export const host = "/api";

export const getAllImages = () =>
    fetch(`${host}/image`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem("token")}`
        },
    }).then(res => {
        let data = res.json();
        if (res.status >= 200 && res.status < 300) {
            return data;
        } else {
            return data.then(err => { throw err; });
        }
    });