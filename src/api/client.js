import axios from 'axios';
import {getToken} from '../utils/helpers';
import { host } from '../api/image'

let Authorization;

if(getToken()) {
    console.log(getToken())
    Authorization = { Authorization: `Bearer ${getToken()}`};
}

export const http = axios.create({
    baseURL: `${host}`,
    headers: { ...Authorization },
})